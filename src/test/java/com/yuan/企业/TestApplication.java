package com.yuan.企业;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.yuan.mapper.AxdAppUserMapper;
import com.yuan.mapper.AxdCarDriverMapper;
import com.yuan.mapper.RecordCarWithPersonInfoMapper;
import com.yuan.pojo.*;
import com.yuan.pojo.车辆基础信息.AxdCarBaseInfo;
import com.yuan.service.AxdAppUserService;
import com.yuan.service.SysUserPostService;
import com.yuan.service.车辆.AxdCarBaseInfoService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yuan.dataupload.Vehicle;
import com.yuan.mapper.AxdRiskTwoDimensionalRecordMapper;
import com.yuan.mapper.企业.AxdFirmMapper;
import com.yuan.pojo.企业.AxdFirm;
import com.yuan.service.VehicleService;
import org.junit.platform.commons.util.StringUtils;
import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/*
 *  @author 雨安
 *  类名： TestApplication
 *  创建时间：2024/6/5
 */

//写执行，入到二维表（执行前truncate本地二维表。之后拷贝insert语句到远端执行（id为null）。
@SpringBootTest
public class TestApplication {

    @Autowired
    private AxdCarBaseInfoService axdCarBaseInfoService;

    @Resource
    private VehicleService vehicleService;

    @Resource
    private AxdRiskTwoDimensionalRecordMapper axdRiskTwoDimensionalRecordMapper;

    @Resource
    private AxdFirmMapper axdFirmMapper;

    @Test
    public void haha() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        List<Vehicle> vehicleList = vehicleService.list(null);

        //遍历数据
        for (Vehicle vehicle : vehicleList) {
            //判断遍历对象不为 null
            if (vehicle != null && StringUtils.isNotBlank(vehicle.getImplementationDate())){
                //创建二维表对象
                AxdRiskTwoDimensionalRecord axd = new AxdRiskTwoDimensionalRecord();
                //设置车牌号
                axd.setCarNo(vehicle.getCarNo());
                //设置维护日期
                axd.setMaintenanceDate(sdf.parse(vehicle.getImplementationDate()));
                //设置企业id
                AxdFirm axdFirm = axdFirmMapper.selectOne(new QueryWrapper<AxdFirm>().lambda()
                        .like(AxdFirm::getName, vehicle.getCoName())
                        .eq(AxdFirm::getDeleteFlag, Boolean.FALSE)
                        .last("limit 1")
                );
                //设置图片
                if (vehicle.getImgUri() != null){
                    axd.setPic(vehicle.getImgUri());
                }

                //设置企业id
                if (axdFirm != null){
                    //企业id
                    axd.setFirmId(axdFirm.getId());
                    //企业名
                    axd.setFirmName(axdFirm.getName());
                    //设置默认值
                    axd.setCreateBy("admin");
                    axd.setUpdateBy("admin");
                    axd.setCreateTime(new Date());
                    axd.setUpdateTime(new Date());
                    axd.setDeleteFlag(Boolean.FALSE);
                    //执行添加
                    axdRiskTwoDimensionalRecordMapper.insert(axd);
                }else{
                    continue;
                }

            }

        }//for each 结束

        vehicleList.forEach(System.out::println);
    }



    @Resource
    private SysUserPostService sysUserPostService;

    @Resource
    private AxdAppUserService axdAppUserService;

    @Resource
    private AxdAppUserMapper axdAppUserMapper;

    @Resource
    private AxdCarDriverMapper axdCarDriverMapper;

    @Resource
    private RecordCarWithPersonInfoMapper recordCarWithPersonInfoMapper;

    @Test
    void hahaaa(){

        LambdaQueryWrapper<AxdAppUser> queryWrapper = new LambdaQueryWrapper<>();
        //查询身份证重复的数据
        queryWrapper.select(AxdAppUser::getIdNumber)
                .groupBy(AxdAppUser::getIdNumber)
                .having("count(*) > 1");
        //拿到指定数据
        List<Map<String, Object>> result = axdAppUserMapper.selectMaps(queryWrapper);

        for (Map<String, Object> map : result) {
            if (map!=null){
                //转换重复的身份证号
                String idNumber = (String) map.get("id_number");
                //使用身份证号搜索所有的数据
                List<AxdAppUser> list = axdAppUserService.list(new LambdaQueryWrapper<AxdAppUser>()
                        .eq(AxdAppUser::getIdNumber, idNumber)
                        .orderByDesc(AxdAppUser::getUpdateTime)
                );

                //遍历集合对象
                System.out.println("这是我的遍历测试");
                //将遍历的集合数据的userid取出来进行查询
                List<Long> collect = list.stream().map(ele -> ele.getUserId()).collect(Collectors.toList());
                //查询sys user post表
                List<SysUserPost> sysUserPostList = sysUserPostService.list(new LambdaQueryWrapper<SysUserPost>()
                        .in(SysUserPost::getUserId, collect)
                );
                //遍历集合
                sysUserPostList.forEach(ele->{
                    System.out.println(collect.get(0) + "这是我想设置的userid" + ele);
                    //判断操作的对象不是要保存的最新数据
                    if (ele.getUserId().equals(collect.get(0)) == false){

                        System.out.println("进入判断" + ele + "这是我的最新数对象：" + collect.get(0));
                        //使用ele去查询数据 查询当前遍历的数据对象是否有该权限 没有则进行添加设置
                        SysUserPost one = sysUserPostService.getOne(new LambdaQueryWrapper<SysUserPost>()
                                .eq(SysUserPost::getUserId, collect.get(0))
                                .eq(SysUserPost::getPostId, ele.getPostId())
                                .eq(SysUserPost::getFirmId, ele.getFirmId())
                        );

                        //判断是否存在该权限 如果存在则不进行修改，该权限给最新数据
                        if (one == null){
                            //在设置新权限之前删除原来的
                            sysUserPostService.remove(new LambdaQueryWrapper<SysUserPost>()
                                    .eq(SysUserPost::getUserId, ele.getUserId())
                                    .eq(SysUserPost::getPostId, ele.getPostId())
                                    .eq(SysUserPost::getFirmId, ele.getFirmId()));
                            //将userid设置成最近更新的那条数据
                            ele.setUserId(collect.get(0));
                            //如果为null则进行数据添加 为该最新用户添加以前的权限
                            System.out.println("这是我添加的权限数据" + ele);
                            sysUserPostService.save(ele);
                        }else{
                            System.out.println("进入了直接删除");
                            //不管发生什么都将不是现在使用的人的权限给删除了
                            sysUserPostService.remove(new LambdaQueryWrapper<SysUserPost>()
                                    .eq(SysUserPost::getUserId, ele.getUserId())
                                    .eq(SysUserPost::getPostId, ele.getPostId())
                                    .eq(SysUserPost::getFirmId, ele.getFirmId()));
                        }

                    }


                });



                System.out.println("这是我原来的数据：" + collect);
                //将集合的第一条数据进行抹除
                collect.remove(0);
                System.out.println("这是我移除后的数据：" + collect);
                //删除前判断是否删除的对象存在人车匹配数据
                List<AxdCarDriver> axdCarDriverList = axdCarDriverMapper.selectList(new LambdaQueryWrapper<AxdCarDriver>()
                        .in(AxdCarDriver::getEmployeeId, collect)
                        .eq(AxdCarDriver::getDeleteFlag, Boolean.FALSE)
                );
                //如果存在人车匹配数据则进行保存
                if (Objects.nonNull(axdCarDriverList) && axdCarDriverList.size() > 0){
                    //通过人员id找到对应的人保存身份证找到对应的车保留车牌号
                    for (AxdCarDriver axdCarDriver : axdCarDriverList) {

                        if (Objects.nonNull(axdCarDriver)){
                            //创建数据保留对象
                            RecordCarWithPersonInfo recordCarWithPersonInfo = new RecordCarWithPersonInfo();
                            //查询指定的人
                            AxdAppUser axdAppUser = axdAppUserMapper.selectById(axdCarDriver.getEmployeeId());
                            //查询指定的车
                            AxdCarBaseInfo axdCarBaseInfo = axdCarBaseInfoService.getById(axdCarDriver.getCarId());
                            //设置数据
                            recordCarWithPersonInfo.setCarId(axdCarBaseInfo.getId());
                            recordCarWithPersonInfo.setIdNumber(axdAppUser.getIdNumber().toString());
                            //执行保留的人车数据
                            System.out.println("这是我的保留的人车数据：" + recordCarWithPersonInfo);
                            //执行添加操作
                            recordCarWithPersonInfoMapper.insert(recordCarWithPersonInfo);
                        }//判断不为 null

                    }//for each 结束

                }//非 null 判断

                //将其他数据进行逻辑删除
                boolean b = axdAppUserService.removeByIds(collect);

                System.out.println(b + "这是我的删除结果");

                System.out.println(collect + "我是测试哈哈" + collect.get(0));


            }

        }//for each结束 遍历有多个数据的身份证数据对象

    }








    @Test
    public void test(){
        try{
            //测试查看
            List<RecordCarWithPersonInfo> recordCarWithPersonInfos = recordCarWithPersonInfoMapper.selectList(null);
            //暂时记录没有查询到的身份证集合
            List<String> idList = new ArrayList<>();
            //数据恢复测试
            for (RecordCarWithPersonInfo recordCarWithPersonInfo : recordCarWithPersonInfos) {
                //判断不为 null
                if (recordCarWithPersonInfo != null) {

                    System.out.println(recordCarWithPersonInfo.getIdNumber() + "这是我查询的身份证");
                    //通过身份证查询人
                    AxdAppUser axdAppUser = axdAppUserMapper.selectOne(new LambdaQueryWrapper<AxdAppUser>()
                            .eq(AxdAppUser::getIdNumber, recordCarWithPersonInfo.getIdNumber())
                            .eq(AxdAppUser::getDeleteFlag, Boolean.FALSE)
                            .last("limit 1")
                    );
                    System.out.println(axdAppUser + "这是我查询到的人");
                    //查询车辆
                    AxdCarBaseInfo carBaseInfo = axdCarBaseInfoService.getById(recordCarWithPersonInfo.getCarId());
                    if (axdAppUser!=null){
                        //添加人车匹配数据
                        axdCarDriverMapper.insert(new AxdCarDriver(carBaseInfo.getId(), axdAppUser.getUserId(), Boolean.FALSE));
                    }else{
                        idList.add(recordCarWithPersonInfo.getIdNumber());
                    }
                }

                System.out.println(idList);
            }
        }catch (Exception e){
            e.printStackTrace();
        }


    }


    /**
     * [500110198611280834, 500234198603032458, 510231197907141918, 510230197309104117, 51022819740126041X, 422802199103183442, 511623198812172532, 510282198203012119, 452128198111141011, 510222196711239031, 510230197306263438, 510230197204174653, 510230196611101713, 510230197108016735, 510231197902221513, 510224196602270710, 510230197005154115, 510230197703213733, 510230196912153939, 500225199409044170, 510521197707090718, 500225198908033316, 510230196312184117, 510521197502181915, 510230196809264112, 500226198608041513, 500225199105150353, 500225199001131115, 510282197811053470, 500225198706213810, 511023198502055178, 512527198105181211, 511023199210040278, 510221197906155610, 500225198907299057, 510230197210084136, 510231197407012415, 510230197904013834, 510230197012303895, 500382198906244673, 511023199102095335, 510227197205291757, 500225199607290735, 500226198612123116, 500113198610069139, 513225197210113177, 500384198708052438, 510221198307263817, 512301197212213451, 522322198712042415, 522528197703080414, 520202198711060416, 500225199708124314, 500225198701230718, 500225199008203812, 513123197005080615, 510230198306261111, 500101199407278073, 510223197303051436]
     */





}
