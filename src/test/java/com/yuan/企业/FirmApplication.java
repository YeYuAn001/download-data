package com.yuan.企业;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.yuan.pojo.dto.ExcelDto;
import com.yuan.pojo.企业.AxdFirm;
import com.yuan.service.企业.AxdFirmService;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/*
 *  @author 雨安
 *  类名： FirmApplication
 *  创建时间：2024/3/28
 */
@SpringBootTest
public class FirmApplication extends AnalysisEventListener<ExcelDto> {

    @Autowired
    AxdFirmService axdFirmService;
    @Test
    void ee() throws ParseException {
        //设置读取的文件地址
        String fileName = "D:\\Spring框架内容学习所有\\Netty_Demo2\\src\\main\\resources\\企业.xlsx";
        //渎职 1.文件名 2.转换实体类 3.监听对象
        ExcelReader reader = EasyExcel.read(fileName, ExcelDto.class,new FirmApplication())
                //构建
                .build();
        // 读取Excel文件中的数据
        reader.read();
        // 关闭reader
        reader.finish();

        //创建日期转换对象
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        //创建执行sql批量添加的处理集合
        List<AxdFirm> axdList = new ArrayList<>();

        //遍历集合
        for (int i = 0; i < aList.size(); i++) {
            //判断当前遍历对象不为null
            if (aList.get(i)!=null){
                //创建实体对象
                AxdFirm axd = new AxdFirm();
                //判断业户名称不为null
                if (StringUtils.isNotBlank(aList.get(i).getYhmc())){
                    //设置业户名称
                    axd.setName(aList.get(i).getYhmc());
                }
                //判断企业简称
                if (StringUtils.isNotBlank(aList.get(i).getQyjc())){
                    //设置企业简称
                    axd.setShortName(aList.get(i).getQyjc());
                }
                //判断经营许可证号
                if (StringUtils.isNotBlank(aList.get(i).getJyxkzh())){
                    //设置经营许可证号
                    axd.setLicnum(aList.get(i).getJyxkzh());
                }
                //判断设置通讯地址
                if (StringUtils.isNotBlank(aList.get(i).getTxdz())){
                    //设置通讯地址
                    axd.setAddress(aList.get(i).getTxdz());
                }
                //判断负责人姓名
                if (StringUtils.isNotBlank(aList.get(i).getFzrxm())){
                    //设置负责人姓名
                    axd.setLegal(aList.get(i).getFzrxm());
                }
                //判断经济性质
                if (StringUtils.isNotBlank(aList.get(i).getJjxz())){
                    //设置经济类型
                    axd.setEconomicType(aList.get(i).getJjxz());
                }
                //判断许可证结束日期
                if (StringUtils.isNotBlank(aList.get(i).getZjyxqjs())){
                    //设置经营许可证有效期
                    axd.setLiceTime(sdf.parse(aList.get(i).getZjyxqjs()));
                }

                //设置一些默认信息
                axd.setCreateBy("admin");
                axd.setCreateTime(new Date());
                axd.setUpdateBy("admin");
                axd.setUpdateTime(new Date());
                //待审核
                axd.setAuditStatus(0);
                //设置注册时间
                axd.setRegisterTime(new Date());
                //设置是否逻辑删除
                axd.setDeleteFlag(false);
                //设置企业类型
                axd.setFirmType("FIRM");
                //设置是否被管理
                axd.setGroupSelected(false);
                //将数据添加到集合中
                axdList.add(axd);
            }
        }
        //执行添加操作
        axdFirmService.saveBatch(axdList);
    }

    //创建静态集合
    private static List<ExcelDto> aList = new ArrayList<>();

    //实现eazy excel继承的方法实现重写
    @Override
    public void invoke(ExcelDto data, AnalysisContext context) {
        //拿到数据集合
        aList.add(data);
    }

    //实现eazy excel继承的方法实现重写
    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        System.out.println("读取完毕。。。。");
    }

    @Test
    void ea() throws UnsupportedEncodingException {
        String fileName = "D:\\Spring框架内容学习所有\\Netty_Demo2\\src\\main\\resources\\企业.xlsx";

        List<ExcelDto> lst =  new ArrayList<>();//空数组作为导入模板 可以再写一个方法给数组赋值作为导出模板

        //表头字体颜色map 1为user中索引位置
        Map<Integer,Short> colorMap=new HashMap<>();
        colorMap.put(1, IndexedColors.BLUE.index);

        List<ExcelDto> list = new ArrayList<>();

        EasyExcel.write(fileName, ExcelDto.class).sheet("企业信息数据").doWrite(list);
    }

}
