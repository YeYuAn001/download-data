//package com.yuan.车辆;
//
//import com.alibaba.excel.EasyExcel;
//import com.alibaba.excel.ExcelReader;
//import com.alibaba.excel.context.AnalysisContext;
//import com.alibaba.excel.event.AnalysisEventListener;
//import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
//import com.yuan.mapper.企业.AxdFirmMapper;
//import com.yuan.mapper.车辆.AxdCarBaseInfoMapper;
//import com.yuan.pojo.dto.CarExcelDto;
//import com.yuan.pojo.vo.CarTechnology;
//import com.yuan.pojo.企业.AxdFirm;
//import com.yuan.pojo.车辆基础信息.AxdCarBaseInfo;
//import com.yuan.pojo.车辆基础信息.AxdCarTechnologyInfo;
//import com.yuan.service.车辆.AxdCarBaseInfoService;
//import com.yuan.service.车辆.AxdCarTechnologyInfoService;
//import org.apache.commons.lang3.StringUtils;
//import org.apache.poi.ss.usermodel.IndexedColors;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import java.io.UnsupportedEncodingException;
//import java.util.*;
//
///*
// *  @author 雨安
// *  类名： CarAppliation
// *  创建时间：2024/3/28
// */
//@SpringBootTest
//public class CarAppliation extends AnalysisEventListener<CarExcelDto> {
//    //装配企业查询
//    @Autowired
//    AxdFirmMapper axdFirmMapper;
//
//    @Autowired
//    AxdCarBaseInfoService axdCarBaseInfoService;
//
//
//    @Autowired
//    AxdCarBaseInfoMapper axdCarBaseInfoMapper;
//
//    @Autowired
//    AxdCarTechnologyInfoService axdCarTechnologyInfoService;
//    @Test
//    void eee(){
//        //拿到当前集合
//        List<CarTechnology> axdList = axdCarBaseInfoMapper.selectALlCarTechnology();
//        //创建执行添加结合
//        List<AxdCarTechnologyInfo> insertList = new ArrayList<>();
//        System.out.println(axdList.size());
//        //遍历当前集合
//        for (int i = 0; i < axdList.size(); i++) {
//            //判断集合不为null
//            if (axdList.get(i)!=null && axdList.get(i).toString().trim().length()!=0){
//                //创建车辆技术等级对象
//                AxdCarTechnologyInfo axd = new AxdCarTechnologyInfo();
//                //判断非null设置车牌号
//                if (org.junit.platform.commons.util.StringUtils.isNotBlank(axdList.get(i).getCarNo())){
//                    //设置车牌号
//                    axd.setCarNo(axdList.get(i).getCarNo());
//                }
//                //判断非null
//                if (org.junit.platform.commons.util.StringUtils.isNotBlank(axdList.get(i).getVehiLength())){
//                    //设置车长che
//                    axd.setCarLength(Integer.parseInt(axdList.get(i).getVehiLength()));
//                }
//                //判断非null
//                if (org.junit.platform.commons.util.StringUtils.isNotBlank(axdList.get(i).getVehiWidth())){
//                    //设置车宽
//                    axd.setCarWidth(Integer.parseInt(axdList.get(i).getVehiWidth()));
//                }
//                //判断非null
//                if (org.junit.platform.commons.util.StringUtils.isNotBlank(axdList.get(i).getVehiHigh())){
//                    //设置车高
//                    axd.setCarHeight(Integer.parseInt(axdList.get(i).getVehiHigh()));
//                }
//                //判断非null
//                if (org.junit.platform.commons.util.StringUtils.isNotBlank(axdList.get(i).getId())){
//                    //设置车辆id
//                    axd.setCarId(Long.parseLong(axdList.get(i).getId()));
//                }
//                //设置创建时间
//                axd.setCreateTime(new Date());
//                //设置创建人
//                axd.setCreateBy("admin");
//                //设置修改时间
//                axd.setUpdateTime(new Date());
//                //设置修改人
//                axd.setUpdateBy("admin");
//                //将数据添加到集合中
//                insertList.add(axd);
//            }
//        }//遍历结束
//        //执行添加sql
//        axdCarTechnologyInfoService.saveBatch(insertList);
//    }
//
//    @Test
//    void test(){
//        //设置读取的文件地址
//        String fileName = "D:\\Spring框架内容学习所有\\Netty_Demo2\\src\\main\\resources\\车辆.xlsx";
//        //渎职 1.文件名 2.转换实体类 3.监听对象
//        ExcelReader reader = EasyExcel.read(fileName, CarExcelDto.class,new CarAppliation())
//                //构建
//                .build();
//        // 读取Excel文件中的数据
//        reader.read();
//        // 关闭reader
//        reader.finish();
//
//        //创建执行添加sql集合对象
//        List<AxdCarBaseInfo> axdList = new ArrayList<>();
//
//        //遍历数据
//        for (int i = 0; i < excelList.size(); i++) {
//            //判断当前遍历不为null
//            if (excelList.get(i)!=null){
//                //创建实体对象
//                AxdCarBaseInfo axd = new AxdCarBaseInfo();
//                //判断业户名称不为null
//                if (StringUtils.isNotBlank(excelList.get(i).getYhmc())){
//                    //创建条件构造器
//                    QueryWrapper<AxdFirm> wrapper = new QueryWrapper<>();
//                    //设置条件
//                    wrapper.eq("name",excelList.get(i).getYhmc());
//                    //拿到查询企业对象
//                    AxdFirm axdFirm = axdFirmMapper.selectOne(wrapper);
//                    //判断企业不为null
//                    if (axdFirm!=null && axdFirm.toString().trim().length()!=0){
//                        //设置公司名称
//                        axd.setFirmId(axdFirm.getId());
//                    }
//                }
//                //判断车辆类别
//                if (StringUtils.isNotBlank(excelList.get(i).getCllb())){
//                    if (excelList.get(i).getCllb().equals("货运车辆")){
//                        //此处不知道货运是什么暂时设置0
//                        axd.setCarCategory(0);
//                    }
//                }
//                //判断车牌号码
//                if (StringUtils.isNotBlank(excelList.get(i).getCphm())){
//                    //设置车牌号
//                    axd.setCarNo(excelList.get(i).getCphm());
//                }
//                //判断车架号
//                if (StringUtils.isNotBlank(excelList.get(i).getVin())){
//                    //设置车架号
//                    axd.setVin(excelList.get(i).getVin());
//                }
//                //判断营运状态
//                if (StringUtils.isNotBlank(excelList.get(i).getYyzt())){
//                    //设置车辆运营状态编码 #  10 正常  80 注销  32 转出  21 停运
//                    if (excelList.get(i).getYyzt().equals("营运"))
//                        axd.setCarStatus(1);
//                    else if (excelList.get(i).getYyzt().equals("停运"))
//                        axd.setCarStatus(0);
//                }
//
//                //判断总质量
//                if (StringUtils.isNotBlank(excelList.get(i).getZzl())){
//                    //设置总质量
//                    axd.setWeight(Integer.parseInt(excelList.get(i).getZzl().replaceAll(",","")));
//                }
//
//                //设置默认值
//                axd.setCreateBy("admin");
//                axd.setCreateTime(new Date());
//                axd.setUpdateBy("admin");
//                axd.setUpdateTime(new Date());
//                axd.setDeleteFlag(false);
//                //将数据添加到集合中
//                axdList.add(axd);
//            }
//        }//for退出
//        //添加到数据库中
//        axdCarBaseInfoService.saveBatch(axdList);
//    }
//
//    //创建静态集合
//    private static List<CarExcelDto> excelList = new ArrayList<>();
//
//    @Override
//    public void invoke(CarExcelDto carExcelDto, AnalysisContext analysisContext) {
//        System.out.println(carExcelDto);
//        excelList.add(carExcelDto);
//    }
//
//    @Override
//    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
//        System.out.println("数据读取完毕。。。");
//    }
//
//
//
//
//    //创建excel文件
//    @Test
//    void ea() throws UnsupportedEncodingException {
//        String fileName = "D:\\Spring框架内容学习所有\\Netty_Demo2\\src\\main\\resources\\车辆.xlsx";
//
//        //表头字体颜色map 1为user中索引位置
//        Map<Integer,Short> colorMap=new HashMap<>();
//        colorMap.put(1, IndexedColors.BLUE.index);
//
//        List<CarExcelDto> list = new ArrayList<>();
//
//        EasyExcel.write(fileName, CarExcelDto.class).sheet("车辆信息数据").doWrite(list);
//    }
//
//}
