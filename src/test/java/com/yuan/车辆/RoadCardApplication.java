//package com.yuan.车辆;
//
//import com.alibaba.excel.EasyExcel;
//import com.alibaba.excel.ExcelReader;
//import com.alibaba.excel.context.AnalysisContext;
//import com.alibaba.excel.event.AnalysisEventListener;
//import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
//import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
//import com.yuan.mapper.车辆.AxdCarBaseInfoMapper;
//import com.yuan.pojo.dto.CarExcelDto;
//import com.yuan.pojo.dto.RoadExcel;
//import com.yuan.pojo.车辆基础信息.AxdCarBaseInfo;
//import com.yuan.pojo.车辆基础信息.AxdCarTransportInfo;
//import com.yuan.service.车辆.AxdCarTransportInfoServicce;
//import org.apache.poi.ss.usermodel.IndexedColors;
//import org.junit.jupiter.api.Test;
//import org.junit.platform.commons.util.StringUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//
//import java.io.UnsupportedEncodingException;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.*;
//
///*
// *  @author 雨安
// *  类名： RoadCardApplication
// *  创建时间：2024/3/29
// */
//@SpringBootTest
//public class RoadCardApplication  extends AnalysisEventListener<RoadExcel>{
//
//    //创建excel文件
//    @Test
//    void ea() throws UnsupportedEncodingException {
//        String fileName = "D:\\Spring框架内容学习所有\\Netty_Demo2\\src\\main\\resources\\道路运输信息.xlsx";
//
//        //表头字体颜色map 1为user中索引位置
//        Map<Integer,Short> colorMap=new HashMap<>();
//        colorMap.put(1, IndexedColors.BLUE.index);
//
//        List<RoadExcel> list = new ArrayList<>();
//
//        EasyExcel.write(fileName, RoadExcel.class).sheet("道路运输信息数据").doWrite(list);
//    }
//
//
//    //道路运输
//    @Autowired
//    AxdCarTransportInfoServicce axdCarTransportInfoServicce;
//
//    @Autowired
//    AxdCarBaseInfoMapper axdCarBaseInfoMapper;
//
//    //创建静态集合变量
//    private static List<RoadExcel> excelList = new ArrayList<>();
//
//
//    @Override
//    public void invoke(RoadExcel roadExcel, AnalysisContext analysisContext) {
//        System.out.println(roadExcel);
//        excelList.add(roadExcel);
//    }
//
//    @Override
//    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
//        System.out.println("读取完毕。。。");
//    }
//
//
//    //设置发动机号
//    @Test
//    void test() throws ParseException {
//        //设置读取的文件地址
//        String fileName = "D:\\Spring框架内容学习所有\\Netty_Demo2\\src\\main\\resources\\道路运输信息.xlsx";
//        //渎职 1.文件名 2.转换实体类 3.监听对象
//        ExcelReader reader = EasyExcel.read(fileName, RoadExcel.class, new RoadCardApplication())
//                //构建
//                .build();
//        // 读取Excel文件中的数据
//        reader.read();
//        // 关闭reader
//        reader.finish();
//
//        //遍历集合数据
//        for (int i = 0; i < excelList.size(); i++) {
//            //判断当前遍历对象不为null
//            if (excelList.get(i)!=null && StringUtils.isNotBlank(excelList.get(i).getFadongjihao())){
//                //判断车牌不为null
//                if (StringUtils.isNotBlank(excelList.get(i).getCarNo())){
//                    System.out.println("进入的发动机号设置：" + excelList.get(i).getFadongjihao() + "查询的车牌为：" + excelList.get(i).getCarNo());
//                    //创建条件构造器
//                    QueryWrapper<AxdCarBaseInfo> wrapper = new QueryWrapper<>();
//                    //设置条件
//                    wrapper.eq("car_no",excelList.get(i).getCarNo());
//                    //查询对象
//                    AxdCarBaseInfo axdCarBaseInfo = axdCarBaseInfoMapper.selectOne(wrapper);
//                    //判断查询对象不为null
//                    if (axdCarBaseInfo!=null && axdCarBaseInfo.toString().trim().length()!=0){
//                        System.out.println("进入了修改："+axdCarBaseInfo.getCarNo() + "编号为：" + axdCarBaseInfo.getId());
//                        axdCarBaseInfo.setEngineNo(excelList.get(i).getFadongjihao());
//                        //执行修改
//                        axdCarBaseInfoMapper.updateById(axdCarBaseInfo);
//                    }
//
//                }
//
//            }
//        }//for循环结束
//    }
//
//    /*
//    //道路运输证号数据
//    @Test
//    void test() throws ParseException {
//        //设置读取的文件地址
//        String fileName = "D:\\Spring框架内容学习所有\\Netty_Demo2\\src\\main\\resources\\道路运输信息.xlsx";
//        //渎职 1.文件名 2.转换实体类 3.监听对象
//        ExcelReader reader = EasyExcel.read(fileName, RoadExcel.class, new RoadCardApplication())
//                //构建
//                .build();
//        // 读取Excel文件中的数据
//        reader.read();
//        // 关闭reader
//        reader.finish();
//
//        //创建添加sql集合
//        List<AxdCarTransportInfo> axdList = new ArrayList<>();
//        //创建日期对象
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//
//        //遍历集合数据
//        for (int i = 0; i < excelList.size(); i++) {
//            //判断数据不为null
//            if (excelList.get(i)!=null){
//                //创建实体对象
//                AxdCarTransportInfo axd = new AxdCarTransportInfo();
//                //设置车牌号
//                if (StringUtils.isNotBlank(excelList.get(i).getCarNo())){
//                    axd.setCarNo(excelList.get(i).getCarNo());
//                }
//                //设置道路运输证号
//                if (StringUtils.isNotBlank(excelList.get(i).getDaoluyszh())){
//                    axd.setRoadCertNo(excelList.get(i).getDaoluyszh());
//                }
//                //道路运输证发证时间
//                if (StringUtils.isNotBlank(excelList.get(i).getDaolustartdate())){
//                    axd.setRoadCertTime(sdf.parse(excelList.get(i).getDaolustartdate()));
//                }
//                //设置车辆id
//                if (StringUtils.isNotBlank(excelList.get(i).getCarNo())){
//                    QueryWrapper<AxdCarBaseInfo> wrapper = new QueryWrapper<>();
//                    wrapper.eq("car_no",excelList.get(i).getCarNo());
//                    //查询车辆id
//                    AxdCarBaseInfo axdCarBaseInfo = axdCarBaseInfoMapper.selectOne(wrapper);
//                    //判断车辆不为null
//                    if (axdCarBaseInfo!=null){
//                        //设置车辆id
//                        axd.setCarId(axdCarBaseInfo.getId());
//                    }
//                }
//                //判断年审
//                if (StringUtils.isNotBlank(excelList.get(i).getNianshenenddate())){
//                    axd.setValidUntil(sdf.parse(excelList.get(i).getNianshenenddate()));
//                }
//                //设置默认数据
//                axd.setCreateBy("admin");
//                axd.setCreateTime(new Date());
//                axd.setUpdateBy("admin");
//                axd.setUpdateTime(new Date());
//                axd.setDeleteFlag(false);
//                //添加到集合中将数据
//                axdList.add(axd);
//            }
//        }
//        //执行添加语句
//        axdCarTransportInfoServicce.saveBatch(axdList);
//    }
// */
//
//    }
