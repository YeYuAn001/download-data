//package com.yuan.车辆;
//
//import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
//import com.yuan.mapper.RoadtransportMapper;
//import com.yuan.mapper.车辆.AxdCarBaseInfoMapper;
//import com.yuan.pojo.车辆基础信息.AxdCarBaseInfo;
//import com.yuan.pojo.车辆基础信息.Roadtransport;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//
//import java.util.List;
//
///*
// *  @author 雨安
// *  类名： UpdateCarData
// *  创建时间：2024/3/29
// */
//@SpringBootTest
//public class UpdateCarData {
//
//    //车辆sql
//    @Autowired
//    private AxdCarBaseInfoMapper axdCarBaseInfoMapper;
//
//    //车辆基础信息
//    @Autowired
//    private RoadtransportMapper roadtransportMapper;
//
//    //测试
//    @Test
//    void aa(){
//        //给所有车辆设置颜色
//        List<AxdCarBaseInfo> axdList = axdCarBaseInfoMapper.selectList(null);
//        //遍历车辆集合
//        for (int i = 0; i < axdList.size(); i++) {
//            //判断当前遍历对象不为null
//            if (axdList.get(i)!=null && axdList.get(i).toString().trim().length()!=0){
//                //创建条件构造器
//                QueryWrapper<Roadtransport> wrapper = new QueryWrapper<>();
//                //设置条件
//                wrapper.like("plate_Num",axdList.get(i).getCarNo());
//                //按照时间排序，并且只取第一条
//                wrapper.orderByDesc("card_end_date").last("limit 1");
//                //查询数据
//                Roadtransport road = roadtransportMapper.selectOne(wrapper);
//                //判断长度
//                if (road!=null && road.toString().trim().length()!=0 && road.getPlateNum().length()>=11){
//                    //设置车牌颜色
//                    axdList.get(i).setCarColor(road.getPlateNum().substring(7,11));
//                }
//                //执行修改
//                axdCarBaseInfoMapper.updateById(axdList.get(i));
//            }
//        }
//
//    }
//
//
//}
