package com.yuan;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication//SpringBoot启动类
@MapperScan("com/yuan/mapper")//MybatisPlus扫描
public class NettyDemo2Application {

    public static void main(String[] args) {
        //启动类
        SpringApplication.run(NettyDemo2Application.class, args);
    }

}