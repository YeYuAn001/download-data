package com.yuan.dataupload;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/*
 *  @author 雨安
 *  类名： ErWei
 *  创建时间：2024/6/3
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ErWei {
    //二维时间
    private String implementationDate;

    //图片路径
    private String imgUri;

}
