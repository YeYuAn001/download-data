package com.yuan.dataupload.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.yuan.dataupload.Vehicle;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/*
 *  @author 雨安
 *  类名： DataLoadVo
 *  创建时间：2024/6/3
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataLoadVo {

    //响应编码
    private String  code;

    //响应消息体
    private String msg;

    //响应集合
    private List<Vehicle> data;

    //总行数
    private Long count;
}
