package com.yuan.dataupload;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/*
 *  @author 雨安
 *  类名： DataLoad
 *  创建时间：2024/6/3
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "vehicle")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Vehicle {

    //车牌号码
    private String carNo;

    //下次维护时间
    private String nextMaintenanceTime;

    //公司名
    private String coName;

    //车辆类型
    private String vehicleType;

    //图片地址
    private String imgUri;

    //二维记录
    private String implementationDate;

    //操作人
    private String doName;

    //二维集合数据
    @TableField(exist = false)
    private List<ErWei> records;

}
