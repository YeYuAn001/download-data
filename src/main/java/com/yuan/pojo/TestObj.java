package com.yuan.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/*
 *  @author 雨安
 *  类名： TestObj
 *  创建时间：2024/3/16
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TestObj {
    //员工邮箱
    private String empEmail;
    //身份证
    private String empIdNum;
    //手机号码
    private String empContTel;
    //性别
    private String empSex;
    //真实姓名
    private String empName;
}




