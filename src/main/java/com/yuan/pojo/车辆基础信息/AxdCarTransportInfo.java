package com.yuan.pojo.车辆基础信息;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.*;
/*
*  @author 雨安
*  类名： AxdCarTransportInfo
*  创建时间：2024/3/29
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("axd_car_transport_info")
public class AxdCarTransportInfo {
    @TableId

    private Long id;

    private String carNo;

    private String roadCertNo;

    private Date roadCertTime;

    private Date validUntil;

    private Integer techLevel;

    private String performanceTesting;

    private String annex;

    private Boolean effective;

    private Long carId;

    private Date createTime;

    private String createBy;

    private Date updateTime;

    private String updateBy;

    private Boolean deleteFlag;

    private String techLevelCert;
}
