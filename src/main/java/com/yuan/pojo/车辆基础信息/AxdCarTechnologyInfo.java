package com.yuan.pojo.车辆基础信息;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/*
 *  @author 雨安
 *  类名： AxdCarTechnologyInfo
 *  创建时间：2024/3/25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("axd_car_technology_info")
public class AxdCarTechnologyInfo {
    @TableId
    private Long id; // 主键

    private String carNo; //车牌

    private String regCerNumber; // 登记证书编号

    private String regCerPic; // 登记证书图片

    private String fileNumber; // 档案编号

    private String selfNumber; // 自编号

    private String operationNumber; // 营运编号

    private Boolean isOut; // 是否驻外停放

    private String typeLevel; // 类型等级(一级 二级)

    private String lineMarker; // 线路标志号

    private String wheelbase; // 前后轮距

    private String bodyColor; // 车身颜色

    private Integer tireCount; // 轮胎数

    private Long displacement; // 排量

    private Integer power; // 功率

    private String factoryName; // 厂牌型号

    private Integer carLength; // 车辆尺寸长(毫米)

    private Integer carWidth; // 车辆尺寸宽(毫米)

    private Integer carHeight; // 车辆尺寸高(毫米)

    private String manufacture; // 制造商名称

    private Integer carAxles; // 车轴数/根

    private Long carFuel; // 燃料类型

    private Double carTon; // 吨位

    private Integer rearaxleSpringPlates; // 后轴弹簧片数/片

    private String driverMen; // 驾驶室共乘/座

    @TableField("GPS")
    private Integer GPS; // gps安装情况 (0未安装 1已安装)

    private String commNo; // 通讯卡号

    @TableField("GPS_operators")
    private String GPSOperators; // GPS运营商

    @TableField("GPS_terminal")
    private String GPSTerminal; // GPS监控终端厂牌型号

    @TableField("GPS_install_time")
    private Date GPSInstallTime; // GPS安装时间

    @TableField("GPS_sonline_time")
    private Date GPSSonlineTime; // GPS接入时间

    @TableField("GPS_valid_until")
    private Date GPSValidUntil; // GPS有效期至

    private Boolean triWood; // 是否有三角木 (0否 1是)

    private Boolean extinguisher; // 是否有灭火器 (0否 1是)

    private Double limitSpeed; // 限速

    private Long carId; // 车辆id

    private Date createTime; // 创建时间

    private String createBy; // 创建者

    private Date updateTime; // 更新时间

    private String updateBy; // 更新者

    private Boolean deleteFlag; // 删除标识(0-正常 1-删除)
}
