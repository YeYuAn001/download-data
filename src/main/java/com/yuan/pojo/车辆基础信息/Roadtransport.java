package com.yuan.pojo.车辆基础信息;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


/*
 *  @author 雨安
 *  类名： RoadTransport
 *  创建时间：2024/3/1 道路运输经营许可信息-接口  此处因为使用cloud原因版本不能使用lombok需要手动生成
 */
@JsonIgnoreProperties(ignoreUnknown = true) //自动识别不是字段的不进行映射
public class Roadtransport {
    /**
     * address
     * 地址
     */
    @JsonProperty("ADDRESS")
    private String address;

    /**
     * appNo
     * 申请号
     */
    @JsonProperty("APPNO")
    private String appNo;

    /**
     * busiScope
     * 业务范围
     */
    @JsonProperty("BUSISCOPE")
    private String busiScope;

    /**
     * cardBeginDate
     * 卡开始日期
     */
    @JsonProperty("CARDBEGINDATE")
    private String cardBeginDate;

    /**
     * cardEndDate
     * 卡结束日期
     */
    @JsonProperty("CARDENDDATE")
    private String cardEndDate;

    /**
     * cardId
     * 卡ID
     */
    @JsonProperty("CARDID")
    private String cardId;

    /**
     * economicType
     * 经济类型
     */
    @JsonProperty("ECONOMICTYPE")
    private String economicType;

    /**
     * housName
     * 房名
     */
    @JsonProperty("HOUSNAME")
    private String housName;

    /**
     * isFlag
     * 是否标志
     */
    @JsonProperty("ISFLAG")
    private String isFlag;

    /**
     * labelType
     * 标签类型
     */
    @JsonProperty("LABELTYPE")
    private String labelType;

    /**
     * liceNo
     * 许可号
     */
    @JsonProperty("LICENO")
    private String liceNo;

    /**
     * lssuDate
     * 颁发日期
     */
    @JsonProperty("LSSUDATE")
    private String lssuDate;

    /**
     * lssuUnitName
     * 颁发单位名
     */
    @JsonProperty("LSSUUNITNAME")
    private String lssuUnitName;

    /**
     * plateNum
     * 车牌号
     */
    @JsonProperty("PLATENUM")
    private String plateNum;

    /**
     * seats
     * 座位数
     */
    @JsonProperty("SEATS")
    private String seats;

    /**
     * trafNo
     * 交通号
     */
    @JsonProperty("TRAFNO")
    private String trafNo;

    /**
     * vehiId
     * 车辆ID
     */
    @JsonProperty("VEHIID")
    private String vehiId;

    /**
     * vehiSize
     * 车辆尺寸
     */
    @JsonProperty("VEHISIZE")
    private String vehiSize;

    /**
     * vehiType
     * 车辆类型
     */
    @JsonProperty("VEHITYPE")
    private String vehiType;

    //无参
    public Roadtransport() {
    }

    //有参
    public Roadtransport(String address, String appNo, String busiScope, String cardBeginDate, String cardEndDate, String cardId, String economicType, String housName, String isFlag, String labelType, String liceNo, String lssuDate, String lssuUnitName, String plateNum, String seats, String trafNo, String vehiId, String vehiSize, String vehiType) {
        this.address = address;
        this.appNo = appNo;
        this.busiScope = busiScope;
        this.cardBeginDate = cardBeginDate;
        this.cardEndDate = cardEndDate;
        this.cardId = cardId;
        this.economicType = economicType;
        this.housName = housName;
        this.isFlag = isFlag;
        this.labelType = labelType;
        this.liceNo = liceNo;
        this.lssuDate = lssuDate;
        this.lssuUnitName = lssuUnitName;
        this.plateNum = plateNum;
        this.seats = seats;
        this.trafNo = trafNo;
        this.vehiId = vehiId;
        this.vehiSize = vehiSize;
        this.vehiType = vehiType;
    }

    //toString
    @Override
    public String toString() {
        return "RoadTransport{" +
                "address='" + address + '\'' +
                ", appNo='" + appNo + '\'' +
                ", busiScope='" + busiScope + '\'' +
                ", cardBeginDate='" + cardBeginDate + '\'' +
                ", cardEndDate='" + cardEndDate + '\'' +
                ", cardId='" + cardId + '\'' +
                ", economicType='" + economicType + '\'' +
                ", housName='" + housName + '\'' +
                ", isFlag='" + isFlag + '\'' +
                ", labelType='" + labelType + '\'' +
                ", liceNo='" + liceNo + '\'' +
                ", lssuDate='" + lssuDate + '\'' +
                ", lssuUnitName='" + lssuUnitName + '\'' +
                ", plateNum='" + plateNum + '\'' +
                ", seats='" + seats + '\'' +
                ", trafNo='" + trafNo + '\'' +
                ", vehiId='" + vehiId + '\'' +
                ", vehiSize='" + vehiSize + '\'' +
                ", vehiType='" + vehiType + '\'' +
                '}';
    }


    //getter + setter

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAppNo() {
        return appNo;
    }

    public void setAppNo(String appNo) {
        this.appNo = appNo;
    }

    public String getBusiScope() {
        return busiScope;
    }

    public void setBusiScope(String busiScope) {
        this.busiScope = busiScope;
    }

    public String getCardBeginDate() {
        return cardBeginDate;
    }

    public void setCardBeginDate(String cardBeginDate) {
        this.cardBeginDate = cardBeginDate;
    }

    public String getCardEndDate() {
        return cardEndDate;
    }

    public void setCardEndDate(String cardEndDate) {
        this.cardEndDate = cardEndDate;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getEconomicType() {
        return economicType;
    }

    public void setEconomicType(String economicType) {
        this.economicType = economicType;
    }

    public String getHousName() {
        return housName;
    }

    public void setHousName(String housName) {
        this.housName = housName;
    }

    public String getIsFlag() {
        return isFlag;
    }

    public void setIsFlag(String isFlag) {
        this.isFlag = isFlag;
    }

    public String getLabelType() {
        return labelType;
    }

    public void setLabelType(String labelType) {
        this.labelType = labelType;
    }

    public String getLiceNo() {
        return liceNo;
    }

    public void setLiceNo(String liceNo) {
        this.liceNo = liceNo;
    }

    public String getLssuDate() {
        return lssuDate;
    }

    public void setLssuDate(String lssuDate) {
        this.lssuDate = lssuDate;
    }

    public String getLssuUnitName() {
        return lssuUnitName;
    }

    public void setLssuUnitName(String lssuUnitName) {
        this.lssuUnitName = lssuUnitName;
    }

    public String getPlateNum() {
        return plateNum;
    }

    public void setPlateNum(String plateNum) {
        this.plateNum = plateNum;
    }

    public String getSeats() {
        return seats;
    }

    public void setSeats(String seats) {
        this.seats = seats;
    }

    public String getTrafNo() {
        return trafNo;
    }

    public void setTrafNo(String trafNo) {
        this.trafNo = trafNo;
    }

    public String getVehiId() {
        return vehiId;
    }

    public void setVehiId(String vehiId) {
        this.vehiId = vehiId;
    }

    public String getVehiSize() {
        return vehiSize;
    }

    public void setVehiSize(String vehiSize) {
        this.vehiSize = vehiSize;
    }

    public String getVehiType() {
        return vehiType;
    }

    public void setVehiType(String vehiType) {
        this.vehiType = vehiType;
    }
}
