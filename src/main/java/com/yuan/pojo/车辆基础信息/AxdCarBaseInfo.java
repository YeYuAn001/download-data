package com.yuan.pojo.车辆基础信息;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/*
 *  @author 雨安
 *  类名： AxdCarBaseInfo
 *  创建时间：2024/3/21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("axd_car_base_info")
public class AxdCarBaseInfo {
    @TableId
    private Long id; // 主键

    private String carNo; // 车牌号

    private String carPic; // 车辆照片

    private String carColor; // 车牌颜色

    private Integer carTypeId; // 车辆类型字典

    private Integer carCategory; // 车辆类别字典 (普货、网约车、教练车）

    private String vin; // 车架号

    private Integer businessScope; // 经营范围(1-客运经营 2-专线经营 3-货运经营 4-租赁服务)

    private Date regDay; // 注册登记日期

    private Integer weight; // 总质量(kg)

    private Integer carLoad; // 核定载质量

    private Integer carStatus; // 车辆状态(0-停运 1-运营 2-转出)

    private String engineNo; // 发动机号

    private Integer carPeople; // 核定载客人数

    private Boolean isFilings; // 是否备案(0-否 1-是)

    private Boolean isLoan; // 是否贷款(0-否 1-是)

    private Long firmId; // 企业id

    private Date createTime; // 创建时间

    private String createBy; // 创建者

    private Date updateTime; // 更新时间

    private String updateBy; // 更新者

    private Boolean deleteFlag; // 删除标识(0-正常 1-删除)
}

