package com.yuan.pojo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/*
 *  @author 雨安
 *  类名： CarTechnology
 *  创建时间：2024/3/29
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CarTechnology {

    private String id;

    private String carNo;

    private String engineDischarge;

    private String enginePower;

    private String vehiLength;

    private String vehiWidth;

    private String vehiHigh;

}
