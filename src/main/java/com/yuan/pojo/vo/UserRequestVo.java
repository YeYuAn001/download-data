package com.yuan.pojo.vo;

import com.yuan.pojo.UserRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

/*
 *  @author 雨安
 *  类名： UserRequestVo
 *  创建时间：2024/3/15
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserRequestVo {
    //时间搓
    private String timestamp;
    //Hash值MD5
    private String sign;
    //年
    private String year;
    //月
    private String month;
    //数据
    private List<UserRequest> data;
}
