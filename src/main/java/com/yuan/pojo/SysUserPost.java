package com.yuan.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/*
 *  @author 雨安
 *  类名： SysUserPost
 *  创建时间：2024/6/19
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysUserPost {
    private Long userId;

    private Long postId;

    private Long firmId;

}
