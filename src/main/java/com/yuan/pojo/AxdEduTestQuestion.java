package com.yuan.pojo;


import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("axd_edu_test_question")
public class AxdEduTestQuestion {

    @TableId
    private Long id;

    private Byte questionTypeId;

    private String questionText;

    private String questionCategory;

    private String difficultyLevel;

    private String correctAnswer;

    private Date createTime;

    private Byte questionSpeciesId;

    private String source;

    private Long firmId;

    private String createBy;

    private Date updateTime;

    private String updateBy;

    private Boolean deleteFlag;

    private Long paperId;
}
