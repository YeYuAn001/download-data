package com.yuan.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/*
 *  @author 雨安
 *  类名： UserRequest
 *  创建时间：2024/3/15
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("axd_xuexi")
public class UserRequest {
//    //时间戳
//    private String timestamp;
//    //校验 使用MD5
//    private String sign;
//    //年份
//    private String year;
//    //月份
//    private String month;
    //姓名
//    private String name;
//    //身份证号码
//    private String idNumber;
//    //电话号码
//    private String phone;
//    //培训时常
//    private Integer learningTime;
//    //培训状态
//    private String completeStatus;
//    //企业名称
//    private String company;
//    //学习平台
//    private String studyPlatform;
//    //组织机构代码
//    private String orgCode;
    //年份
    private String year;
    //月份
    private String month;
    //姓名
    private String name;
    //身份证号码
    private String idNumber;
    //电话号码
    private String phone;
    //培训时常
    private Integer learningTime;
    //培训状态
    private String completeStatus;
    //企业名称
    private String company;
    //所在企业组织机构代码
    private String orgCode;
    //学习平台
    private String studyPlatform;
}
