package com.yuan.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/*
 *  @author 雨安
 *  类名： AxdAppUser
 *  创建时间：2024/3/17
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AxdTestAppUser {
    @TableId
    // 用户ID
    @TableField("user_id")
    private Long userId;
    // 部门ID
    @TableField("dept_id")
    private Long deptId;
    // 用户账号
    @TableField("user_name")
    private String userName;
    // 用户昵称
    @TableField("nick_name")
    private String nickName;
    // 用户类型（个体户、企业用户）
    @TableField("user_type")
    private String userType;
    // 用户邮箱
    @TableField("email")
    private String email;
    // 身份证
    @TableField("id_number")
    private String idNumber;
    // 手机号码
    @TableField("phone_number")
    private String phoneNumber;
    // 用户性别（0男 1女 2未知）
    @TableField("sex")
    private char sex;
    // 头像地址
    @TableField("avatar")
    private String avatar;
    // 密码
    @TableField("password")
    private String password;
    // 帐号状态（0正常 1停用）
    @TableField("status")
    private char status;
    // 删除标志（0代表存在 1代表删除）
    @TableField("delete_flag")
    private char deleteFlag;
    // 最后登录IP
    @TableField("login_ip")
    private String loginIp;
    // 最后登录时间
    @TableField("login_date")
    private Date loginDate;
    // 创建者
    @TableField("create_by")
    private String createBy;
    // 创建时间
    @TableField("create_time")
    private Date createTime;
    // 更新者
    @TableField("update_by")
    private String updateBy;
    // 更新时间
    @TableField("update_time")
    private Date updateTime;
    // 备注
    @TableField("remark")
    private String remark;
    // 真实姓名
    @TableField("real_name")
    private String realName;
    // 是否备案(0-否 1-是)
    @TableField("is_filings")
    private Boolean isFilings;
    // 企业id
    @TableField("firm_id")
    private Long firmId;
    // 用户人像图片
    @TableField("user_img")
    private String userImg;
    // 用户签名图片
    @TableField("user_signature")
    private String userSignature;
    // 个体户企业id
    @TableField("individual_firm_id")
    private Long individualFirmId;
    // 审核状态 0-待审核 1-审核通过 2 -审核未通过
    @TableField("audit_status")
    private Integer auditStatus;
    // 审核时间
    @TableField("audit_time")
    private Date auditTime;
    // 审核人
    @TableField("auditor")
    private String auditor;
    // b_validity及c_validity此处删除了
    @TableField("openid")
    private String openid;
    // 安全管理人员
    @TableField("credentials")
    private String credentials;
}
