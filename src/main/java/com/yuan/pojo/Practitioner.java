package com.yuan.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/*
 *  @author 雨安
 *  类名： Practitioner
 *  创建时间：2024/3/18
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Practitioner {
    private String appNo;
    private String approve;
    private String archivesStatus;
    private String auditUnit;
    private String belongUnit;
    private String businessArea;
    private String businessType;
    private String cardIssueUnit;
    private String creater;
    private String createTime;
    private String createUnit;
    private String cId;
    private String dataNo;
    private String driveCardNum;
    private String empAddress;
    private String empBirth;
    private String empContTel;
    private String empEduExp;
    private String empEmail;
    private String empHealth;
    private String empIdNum;
    private String empIdType;
    private String employeeCardDate;
    private String employeeId;
    private String employeeNo;
    private String empMobile;
    private String empName;
    private String empNation;
    private String empNative;
    private String empPostcode;
    private String empProduty;
    private String empSex;
    private String fileNo;
    private String fstGetCardDate;
    private String integrityScores;
    private String isLock;
    private String operateTime;
    private String operateUnit;
    private String operator;
    private String ownerId;
    private String remark;
    private String valDateBegin;
    private String valDateEnd;
    private String validtePeriod;
}
