package com.yuan.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.io.Serializable;


@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "axd_risk_two_dimensional_record")
public class AxdRiskTwoDimensionalRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Long id;

    private String carNo;

    private Date maintenanceDate;

    private Integer cycle;

    private Date validityDate;

    private String maintenanceUnit;

    private String contractNumber;

    private String certificateNumber;

    private String testNumber;

    private Boolean qualified;

    private Date filingDate;

    private String registrant;

    private Integer outageDays;

    private String pic;

    private String unitDesc;

    private String appendJob;

    private String mainJob;

    private String remark;

    private Long firmId;

    private String firmName;

    private Date createTime;

    private String createBy;

    private Date updateTime;

    private String updateBy;

    private Boolean deleteFlag;


}
