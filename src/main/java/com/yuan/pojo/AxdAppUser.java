package com.yuan.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
/*
 *  @author 雨安
 *  类名： AxdAppUser
 *  创建时间：2024/6/19
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AxdAppUser {
    /**
     * 用户ID
     */
    @TableId(type = IdType.AUTO)
    private Long userId;
    /**
     * 部门ID
     */
    private Long deptId;
    /**
     * 用户账号
     */
    private String userName;
    /**
     * 用户昵称
     */
    private String nickName;
    /**
     * 用户类型（个体户、企业用户）
     */
    private String userType;
    /**
     * 用户邮箱
     */
    private String email;
    /**
     * 身份证
     */
    private String idNumber;
    /**
     * 电话号码
     */
    private String phonenumber;
    /**
     * 用户性别（0男 1女 2未知）
     */
    private String sex;
    /**
     * 头像地址
     */
    private String avatar;
    /**
     * 密码
     */
    private String password;
    /**
     * 帐号状态（0正常 1停用）
     */
    private String status;
    /**
     * 删除标志（0代表存在 1代表删除）
     */
    private String deleteFlag;
    /**
     * 最后登录IP
     */
    private String loginIp;
    /**
     * 最后登录时间
     */
    private Date loginDate;
    /**
     * 创建者
     */
    private String createBy;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新者
     */
    private String updateBy;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 备注
     */
    private String remark;
    /**
     * 真实姓名
     */
    private String realName;
    /**
     * 是否备案(0-否 1-是)
     */
    private Boolean isFilings;
    /**
     * 企业id
     */
    private Long firmId;
    /**
     * 可能存在于多个企业时，第2、3...个firm_id，用，连接。
     */
    private String firmIds;
    /**
     * 曾经经历的企业
     */
    private String firmEver;
    /**
     * 用户人像图片
     */
    private String userImg;
    /**
     * 用户签名图片
     */
    private String userSignature;
    /**
     * 个体户企业id
     */
    private Long individualFirmId;
    /**
     * 审核状态 0-待审核 1- 审核通过  2 -审核未通过
     */
    private Integer auditStatus;
    /**
     * 审核时间
     */
    private Date auditTime;
    /**
     * 审核人
     */
    private String auditor;
    /**
     * openid
     */
    private String openid;
    /**
     * 安全管理人员
     */
    private String credentials;
    /**
     * 子平台（1 綦江，2 璧山）
     */
    private String subPlatform;
    /**
     * 暂时可见密码
     */
    private String adminPassword;
    // Getters and Setters
}