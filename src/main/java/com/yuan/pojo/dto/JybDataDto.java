package com.yuan.pojo.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/*
 *  @author 雨安
 *  类名： JybDataDto
 *  创建时间：2024/4/8  岗前培训检测题
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class JybDataDto {

    private String result;

    private String data;

    private String msg;

    private List<JybData> rows;

    private Integer total;

    private Integer code;


}
