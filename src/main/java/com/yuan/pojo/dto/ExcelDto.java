package com.yuan.pojo.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/*
 *  @author 雨安
 *  类名： ExcelDto
 *  创建时间：2024/3/18
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExcelDto {
    @ExcelProperty(index = 0, value = {"业户名称"})
    private String yhmc;

    @ExcelProperty(index = 1,value = {"企业简称"})
    private String qyjc;

    @ExcelProperty(index = 2,value = {"经营许可证号"})
    private String jyxkzh;

    @ExcelProperty(index = 3,value = {"通信地址"})
    private String txdz;

    @ExcelProperty(index = 4,value = {"负责人姓名"})
    private String fzrxm;

    @ExcelProperty(index = 5,value = {"经济性质"})
    private String jjxz;

    @ExcelProperty(index = 6,value = {"经营范围"})
    private String jyfw;

    @ExcelProperty(index = 7,value = {"核发机关"})
    private String hfjg;

    @ExcelProperty(index = 8,value = {"证件有效期开始"})
    private String zjyxqks;

    @ExcelProperty(index = 9,value = {"证件有效期结束"})
    private String zjyxqjs;

}
