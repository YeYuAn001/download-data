package com.yuan.pojo.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/*
 *  @author 雨安
 *  类名： CarExcelDto
 *  创建时间：2024/3/28
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CarExcelDto {
    @ExcelProperty(index = 0, value = {"业户名称"})
    private String yhmc;
    @ExcelProperty(index = 1, value = {"所属机构"})
    private String ssjg;
    @ExcelProperty(index = 2, value = {"车辆类别"})
    private String cllb;
    @ExcelProperty(index = 3, value = {"车辆(挂车)号"})
    private String cphm;
    @ExcelProperty(index = 4, value = {"车架号"})
    private String vin;
    @ExcelProperty(index = 5, value = {"车辆营运状态"})
    private String yyzt;
    @ExcelProperty(index = 6, value = {"车辆类型"})
    private String cllx;
    @ExcelProperty(index = 7, value = {"吨位"})
    private String dw;
    @ExcelProperty(index = 8, value = {"总质量"})
    private String zzl;
    @ExcelProperty(index = 9, value = {"准牵引质量"})
    private String zqyzl;

}
