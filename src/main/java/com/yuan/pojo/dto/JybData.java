package com.yuan.pojo.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/*
 *  @author 雨安
 *  类名： JybData
 *  创建时间：2024/4/8   岗前培训检测题
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class JybData {
        @JsonProperty("sn")
        private String sn; // 序号
        @JsonProperty("qSn")
        private String qSn; // 问题序号
        @JsonProperty("qType")
        private String qType; // 问题类型
        @JsonProperty("questionDesc")
        private String questionDesc; // 问题描述
        @JsonProperty("url")
        private String url; // 网址
        @JsonProperty("optionA")
        private String optionA; // 选项A
        @JsonProperty("optionB")
        private String optionB; // 选项B
        @JsonProperty("optionC")
        private String optionC; // 选项C
        @JsonProperty("optionD")
        private String optionD; // 选项D
        @JsonProperty("optionE")
        private String optionE; // 选项E
        @JsonProperty("optionF")
        private String optionF; // 选项F
        @JsonProperty("rightOption")
        private String rightOption; // 正确选项
        @JsonProperty("difficultyMark")
        private String difficultyMark; // 难度评分
        @JsonProperty("useNo")
        private String useNo; // 使用次数
        @JsonProperty("rightPer")
        private String rightPer; // 正确百分比
        @JsonProperty("creationDate")
        private String creationDate; // 创建日期
        @JsonProperty("ttName")
        private String ttName; // TT名称
        @JsonProperty("orgId")
        private String orgId; // 组织ID
        @JsonProperty("qqSn")
        private String qqSn; // QQ序号
        @JsonProperty("mark")
        private String mark; // 标记
        @JsonProperty("resultCode")
        private String resultCode; // 结果代码
        @JsonProperty("fromTtSn")
        private String fromTtSn; // 来自TT序号
        @JsonProperty("toTtSn")
        private String toTtSn; // 到TT序号
        @JsonProperty("questionDesc2")
        private String questionDesc2; // 问题描述2
        @JsonProperty("relSn")
        private String relSn; // 关联序号
        @JsonProperty("chName")
        private String chName; // 中文名称
        @JsonProperty("userName")
        private String userName; // 用户名
}
