package com.yuan.pojo.dto;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/*
 *  @author 雨安
 *  类名： UserResponse
 *  创建时间：2024/3/15
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserResponse {
    //返回响应编码
    private int code;
    //返回消息
    private String msg;
    //返回数据
    private String data;
}
