package com.yuan.pojo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.yuan.pojo.IRData;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/*
 *  @author 雨安
 *  类名： IRDataDto
 *  创建时间：2024/3/17
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class IRDataDto {
    @JsonProperty("code")
    private String code;

    @JsonProperty("msg")
    private String msg;

    @JsonProperty("data")
    private List<IRData> data;

    @JsonProperty("count")
    private Integer count;
}
