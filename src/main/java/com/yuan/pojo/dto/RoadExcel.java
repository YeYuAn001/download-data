package com.yuan.pojo.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/*
 *  @author 雨安
 *  类名： RoadExcel
 *  创建时间：2024/3/29
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoadExcel {
    @ExcelProperty(index = 0, value = {"车牌号"})
    private String carNo;
    @ExcelProperty(index = 1, value = {"发动机号"})
    private String fadongjihao;
    @ExcelProperty(index = 2, value = {"车轴数"})
    private String chezhoushul;
    @ExcelProperty(index = 3, value = {"道路运输证号"})
    private String daoluyszh;
    @ExcelProperty(index = 4, value = {"道路运输证有效起始日期"})
    private String daolustartdate;
    @ExcelProperty(index = 5, value = {"道路运输证有效截止日期"})
    private String daoluenddate;
    @ExcelProperty(index = 6, value = {"年审日期"})
    private String nianshenrq;
    @ExcelProperty(index = 7, value = {"年审有效起始日期"})
    private String nianshengstartdate;
    @ExcelProperty(index = 8, value = {"年审有效截止日期"})
    private String nianshenenddate;
    @ExcelProperty(index = 9, value = {"下次年审日期"})
    private String nextnianshen;


}
