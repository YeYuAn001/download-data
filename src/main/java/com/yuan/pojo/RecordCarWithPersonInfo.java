package com.yuan.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RecordCarWithPersonInfo {

    //人员身份证
    private String idNumber;

    //车辆id
    private Long carId;

}
