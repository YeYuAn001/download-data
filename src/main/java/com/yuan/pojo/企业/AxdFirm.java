package com.yuan.pojo.企业;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/*
 *  @author 雨安
 *  类名： AxdTestFirm
 *  创建时间：2024/3/18
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AxdFirm {
    @TableId(type = IdType.AUTO)
    private Long id; // 主键

    private String district; // 区县行政区域代码

    private String inscode; // 企业编号

    private String name; // 企业名称

    private String shortName; // 企业简称

    private String introduce; // 企业介绍

    private String busLicRegisterAddr; // 营业执照注册地

    private Date busLicTime; // 营业执照有效期

    private String busLicImage; // 营业执照图片

    private String business; // 统一社会信用代码

    private String licnum; // 经营许可证编号

    private Date liceTime; // 经营许可证有效期

    private String liceImage; // 经营许可证图片

    private String industryType; // 行业类型

    private String postcode; // 邮政编码

    private String legal; // 法人代表

    private String legalPersonIdcard; // 法人身份信息

    private String personIdcardImage; // 身份证照片

    private String legalPersonPhone; // 法人联系电话

    private String contacts; // 联系人

    private String phone; // 联系电话

    private String area; // 所在地区

    private String address; // 企业地址

    private BigDecimal siteArea; // 场地面积

    private String registerCapital; // 注册资金

    private String economicType; // 经济类型

    private String enterpriseNature; // 企业性质

    private String email; // 企业邮箱

    private String businessScope; // 经营范围

    private Date registerTime; // 注册时间

    private Date auditTime; // 审核时间

    private String auditName; // 审核人

    private Integer auditStatus; // 审核状态 0-待审核 1-审核通过 2 -审核未通过

    private Date liabilityInsuranceTime; // 安全生产责任险有效期

    private String liabilityInsuranceImage; // 安全生产责任险图片

    private Date createTime; // 创建时间

    private Date updateTime; // 更新时间

    private String createBy; // 创建者

    private String updateBy; // 更新者

    private Boolean deleteFlag; //逻辑删除

    private Long promoterId; // 推广人员id

    private String firmType; // 公司类型（INDIVIDUAL:个体户,FIRM:企业用户）

    private String subPlatform; // 綦江平台：QIJIANG，璧山平台：BISHAN；定级平台：TOP

    private Date issuDate; // 许可证颁发日期

    private Boolean groupSelected;
}
