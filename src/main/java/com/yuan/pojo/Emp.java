package com.yuan.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/*
 *  @author 雨安
 *  类名： Emp
 *  创建时间：2024/3/17
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Emp implements Serializable {
    //员工邮箱
    private String empEmail;
    //身份证
    private String empIdNum;
    //手机号码
    private String empContTel;
    //性别
    private String empSex;
    //真实姓名
    private String empName;
}
