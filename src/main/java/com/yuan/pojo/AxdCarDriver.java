package com.yuan.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AxdCarDriver {

    private Long carId;

    private Long employeeId;

    private  Boolean deleteFlag;

}
