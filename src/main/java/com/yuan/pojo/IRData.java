package com.yuan.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/*
 *  @author 雨安
 *  类名： IRData
 *  创建时间：2024/3/17
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class IRData {
    @JsonProperty("coId")
    // 公司ID
    private int coId;
    @JsonProperty("ownerid")
    // 所有者ID
    private int ownerid;
    @JsonProperty("employeeid")
    // 员工ID
    private int employeeid;
    @JsonProperty("coName")
    // 公司名称
    private String coName;
    @JsonProperty("empname")
    // 员工姓名
    private String empname;
    @JsonProperty("empsexname")
    // 员工性别
    private String empsexname;
    @JsonProperty("empnation")
    // 员工民族
    private String empnation;
    @JsonProperty("empidnum")
    // 员工身份证号
    private String empidnum;
    @JsonProperty("empbirth")
    // 员工出生日期
    private String empbirth;
    @JsonProperty("empnative")
    // 员工籍贯
    private String empnative;
    @JsonProperty("employeeno")
    // 员工工号
    private String employeeno;
    @JsonProperty("employeecarddate")
    // 员工证件领取日期
    private String employeecarddate;
    @JsonProperty("drivecardnum")
    // 驾驶证号
    private String drivecardnum;
    @JsonProperty("fstgetcarddate")
    // 首次领证日期
    private String fstgetcarddate;
    @JsonProperty("models")
    // 车辆型号
    private String models;
    @JsonProperty("dataMatching")
    // 数据匹配
    private String dataMatching;
    @JsonProperty("autoMatch")
    // 自动匹配
    private String autoMatch;
    @JsonProperty("empid")
    // 员工ID
    private int empid;
    @JsonProperty("integrityscores")
    // 完整度分数
    private Integer integrityscores;
    @JsonProperty("empidtype")
    // 员工证件类型
    private String empidtype;
    @JsonProperty("emphealth")
    // 员工健康状况
    private String emphealth;
    @JsonProperty("empeduexp")
    // 员工教育经历
    private String empeduexp;
    @JsonProperty("empmobile")
    // 员工手机号码
    private String empmobile;
    @JsonProperty("empconttel")
    // 员工联系电话
    private String empconttel;
    @JsonProperty("empaddress")
    // 员工地址
    private String empaddress;
    @JsonProperty("cardBeginDate")
    // 证件开始日期
    private String cardBeginDate;
    @JsonProperty("cardEndDate")
    // 证件结束日期
    private String cardEndDate;
    @JsonProperty("valDateBegin")
    // 有效日期开始
    private String valDateBegin;
    @JsonProperty("valDateEnd")
    // 有效日期结束
    private String valDateEnd;
}
