package com.yuan.腾讯云上传;

import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.model.PutObjectRequest;
import com.qcloud.cos.model.PutObjectResult;
import com.qcloud.cos.region.Region;
import java.io.File;

/*
 *  @author 雨安
 *  类名： Upload
 *  创建时间：2024/6/3
 */
public class Upload {
    public static void main(String[] args) {
        // 替换为您的腾讯云账号的 SecretId 和 SecretKey
        String secretId = "AKIDzrX0EE0Vnwsd9RWNyKSLxKSLUO2SfdMq";
        String secretKey = "IBuSlrqiR1QipjPsENJ1WoshJhuufEYe";
        // 替换为您的腾讯云 COS 存储桶所在的地域
        String regionName = "ap-chongqing";
        // 替换为您的腾讯云 COS 存储桶名称
        String bucketName = "axd-1256474543";
        // 文件在 COS 中的路径和名称
        String key = "resourcesimage.jpg";
        // 本地 Excel 文件路径
        String filePath = "D:\\梦潮\\数据下载后端\\src\\main\\resources\\resourcesimage.jpg";

        BasicCOSCredentials cred = new BasicCOSCredentials(secretId, secretKey);
        // 设置 COS 存储桶的地域
        Region region = new Region(regionName);
        // 创建 COS 客户端配置
        ClientConfig clientConfig = new ClientConfig(region);
        // 创建 COS 客户端
        COSClient cosClient = new COSClient(cred, clientConfig);
        // 本地文件对象
        File localFile = new File(filePath);
        // 创建上传请求
        PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, key, localFile);
        // 执行上传操作
        PutObjectResult putObjectResult = cosClient.putObject(putObjectRequest);

        // 获取上传文件的访问URL
        String fileUrl = "https://" + bucketName + ".cos." + regionName + ".myqcloud.com/" + key;
        //打印访问的 url
        System.out.println("文件上传成功，访问URL：" + fileUrl);
        // 关闭 COS 客户端
        cosClient.shutdown();
    }
}
