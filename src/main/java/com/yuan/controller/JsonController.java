package com.yuan.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yuan.pojo.AxdEduTestQuestion;
import com.yuan.pojo.AxdTestAppUser;
import com.yuan.pojo.IRData;
import com.yuan.pojo.dto.IRDataDto;
import com.yuan.pojo.dto.JybData;
import com.yuan.pojo.dto.JybDataDto;
import com.yuan.service.AxdEduTestQuestionService;
import com.yuan.service.AxdTestAppUserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/*
 *  @author 雨安
 *  类名： JsonController
 *  创建时间：2024/3/17
 */
@RestController
@CrossOrigin //跨域处理
public class JsonController {
    @Autowired
    AxdTestAppUserService axdAppUserService;


    @Autowired
    private AxdEduTestQuestionService axdEduTestQuestionService;


    @PostMapping(value ="/getJybJson",produces = "application/json; charset=UTF-8")
    public String hehe(@RequestBody String str) throws JsonProcessingException {
        //创建入库集合
        List<AxdEduTestQuestion> axdList = new ArrayList<>();
        //遍历
        ObjectMapper toObj = new ObjectMapper();
        //得到转换对象
        JybDataDto jyb = toObj.readValue(str, JybDataDto.class);
        //得到数据
        List<JybData> jybDataList = jyb.getRows();
        //遍历数据
        for (int i = 0; i < jybDataList.size(); i++) {
            //判断当前遍历对象不为null
            if (jybDataList.get(i) != null) {
                //创建学习实体类
                AxdEduTestQuestion axdEdu = new AxdEduTestQuestion();

                //判断题目是否不为null
                if (StringUtils.isNotBlank(jybDataList.get(i).getQuestionDesc())) {
                    //设置题目为
                    axdEdu.setQuestionText(jybDataList.get(i).getQuestionDesc());
                }

                //判断单选多选类型是否为null
                if (StringUtils.isNotBlank(jybDataList.get(i).getQType())) {
                    //设置题目单选多选or判断
                    axdEdu.setQuestionTypeId(Byte.parseByte(jybDataList.get(i).getQType()));
                }

                //判断题目培训种类
                if (StringUtils.isNotBlank(jybDataList.get(i).getTtName())){
                    //设置题目培训种类
                    axdEdu.setQuestionCategory(jybDataList.get(i).getTtName());
                }

                //判断困难程度
                if (StringUtils.isNotBlank(jybDataList.get(i).getDifficultyMark())){
                    //判断是否为平普通
                    if (jybDataList.get(i).getDifficultyMark().equals("1")){
                        //设置困难程度
                        axdEdu.setDifficultyLevel("2");
                    }
                }

                //设置正确答案
                if (StringUtils.isNotBlank(jybDataList.get(i).getRightOption())){
                    //设置正确答案
                    String[] split = jybDataList.get(i).getRightOption().split("");
                    //创建字符
                    String option = "";
                    //遍历
                    for (String  s:split) {
                        option += s+",";
                    }
                    //得到拼接后的字符串
                    axdEdu.setCorrectAnswer(option);
                }

                //设置创建时间
                axdEdu.setCreateTime(new Date());
                //设置试题来源
                axdEdu.setSource("PLATFORM");
                //设置创建人
                axdEdu.setCreateBy("admin");
                //设置修改人
                axdEdu.setUpdateBy("admin");
                //设置修改时间
                axdEdu.setUpdateTime(new Date());
                //设置是否逻辑删除
                axdEdu.setDeleteFlag(false);
                //将数据添加到集合中
                axdList.add(axdEdu);
            }
        }//fori街胡思
        //执行添加sql
        axdEduTestQuestionService.saveBatch(axdList);
        //返回成功
        return "成功";
    }

    // entity 实体类 对应数据库字段   mapper 对sql操作   dto 除开数据库本身还
    // dao  对应我们的需要实现的方法层  字符串  sdflkdjlkd,fjfsd    split ,  String[] aaa(String a)   impl继承 实现这个dao 方法重写   a split  return String[]
    // 分很多个包  controller  service  entity  dao  mapper  dto  impl  这几个包
    // 主键存在外键才可以存在   外键存在主键则不能被删除
    

    @PostMapping(value ="/getJson",produces = "application/json; charset=UTF-8")
        public String haha(@RequestBody String str) throws JsonProcessingException {
        //遍历
        ObjectMapper toObj = new ObjectMapper();
        //得到转换对象
        IRDataDto ir = toObj.readValue(str, IRDataDto.class);
        //拿到数据对象
        List<IRData> data = ir.getData();
        //创建接收集合
        List<AxdTestAppUser> axdList = new ArrayList<>();
        for (int i = 0; i < data.size(); i++) {
            //判断当前数据不为null
            if (data.get(i)!=null && data.get(i).toString().trim().length()!=0){
                //创建接收数据对象
                AxdTestAppUser axd = new AxdTestAppUser();
                //给username字段赋默认值
                //非空判断
                if (StringUtils.isNotBlank(data.get(i).getEmpidnum())){
                    //身份证号
                    axd.setIdNumber(data.get(i).getEmpidnum());
                }
                //非空判断
                if (StringUtils.isNotBlank(data.get(i).getEmpmobile())) {
                    //手机号码
                    axd.setPhoneNumber(data.get(i).getEmpmobile());
                }
                //非空判断
                if (StringUtils.isNotBlank(data.get(i).getEmpsexname())){
                    //性别
                    if (data.get(i).getEmpsexname().equals("男")){
                        axd.setSex('0');
                    }else if (data.get(i).getEmpsexname().equals("女")){
                        axd.setSex('1');
                    }else {
                        axd.setSex('2');
                    }
                }
                //非空判断
                if (StringUtils.isNotBlank(data.get(i).getEmpname())){
                    //真实姓名
                    axd.setRealName(data.get(i).getEmpname());
                    //设置username
                    axd.setUserName(data.get(i).getEmpname());
                }
                //添加到集合中
                axdList.add(axd);
            }
        }
        //添加到数据库中
        boolean b = axdAppUserService.saveBatch(axdList);
        return b==true?"成功":"失败";
    }







}
