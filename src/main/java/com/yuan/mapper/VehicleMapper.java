package com.yuan.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yuan.dataupload.Vehicle;
import org.springframework.stereotype.Repository;

/*
 *  @author 雨安
 *  类名： VehicleMapper
 *  创建时间：2024/6/3
 */
@Repository
public interface VehicleMapper extends BaseMapper<Vehicle> {
}
