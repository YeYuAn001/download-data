package com.yuan.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yuan.pojo.AxdRiskTwoDimensionalRecord;
import org.springframework.stereotype.Repository;

@Repository
public interface AxdRiskTwoDimensionalRecordMapper extends BaseMapper<AxdRiskTwoDimensionalRecord> {
}
