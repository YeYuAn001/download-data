package com.yuan.mapper;

import com.yuan.pojo.Emp;
import com.yuan.pojo.TestObj;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/*
 *  @author 雨安
 *  类名： TestMapper
 *  创建时间：2024/3/16
 */
@Repository
public interface TestMapper {
    @Select("select emp_email ,emp_id_num ,emp_cont_tel ,\n" +
            "       emp_sex ,emp_name\n" +
            "from practitioner")
    List<TestObj> selectAll();

    @Select("select count(*) from practitioner")
    Integer selectCount();

    @Select("select emp_email ,emp_id_num ,emp_cont_tel ,\n" +
            "       emp_sex ,emp_name \n" +
            "from practitioner")
    List<Emp> selectEmpAll();
}
