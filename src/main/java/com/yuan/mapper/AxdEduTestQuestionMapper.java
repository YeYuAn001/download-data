package com.yuan.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yuan.pojo.AxdEduTestQuestion;
import org.springframework.stereotype.Repository;

@Repository
public interface AxdEduTestQuestionMapper extends BaseMapper<AxdEduTestQuestion> {
}
