package com.yuan.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yuan.pojo.Practitioner;
import org.springframework.stereotype.Repository;

/*
 *  @author 雨安
 *  类名： mapperMapper
 *  创建时间：2024/3/18
 */
@Repository
public interface mapperMapper extends BaseMapper<Practitioner> {
}
