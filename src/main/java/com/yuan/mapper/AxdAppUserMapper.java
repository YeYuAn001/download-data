package com.yuan.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yuan.pojo.AxdAppUser;
import org.springframework.stereotype.Repository;

/*
 *  @author 雨安
 *  类名： AxdAppUserMapper
 *  创建时间：2024/6/19
 */
@Repository
public interface AxdAppUserMapper extends BaseMapper<AxdAppUser> {
}
