package com.yuan.mapper.车辆;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yuan.pojo.车辆基础信息.AxdCarTechnologyInfo;
import org.springframework.stereotype.Repository;

/*
 *  @author 雨安
 *  类名： AxdCarTechnologyInfoMapper
 *  创建时间：2024/3/29
 */
@Repository
public interface AxdCarTechnologyInfoMapper extends BaseMapper<AxdCarTechnologyInfo> {
}
