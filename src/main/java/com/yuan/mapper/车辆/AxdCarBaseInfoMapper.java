package com.yuan.mapper.车辆;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yuan.pojo.vo.CarTechnology;
import com.yuan.pojo.车辆基础信息.AxdCarBaseInfo;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/*
 *  @author 雨安
 *  类名： AxdCarBaseInfoMapper
 *  创建时间：2024/3/28
 */
@Repository
public interface AxdCarBaseInfoMapper extends BaseMapper<AxdCarBaseInfo> {
    //技术等级
    @Select("select car.id,car_no,rt.engine_discharge  ,rt.engine_power,\n" +
            "       rt.vehi_length ,rt.vehi_width ,rt.vehi_high\n" +
            "from axd_car_base_info car\n" +
            "left join roadtransportvehicles rt on car.car_no = rt.vehicle_no\n" +
            "where car_no is not null and rt.vehi_high is not null and rt.vehi_width is not null  and rt.vehi_length is not null")
    List<CarTechnology> selectALlCarTechnology();
}
