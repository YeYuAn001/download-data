package com.yuan.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yuan.pojo.RecordCarWithPersonInfo;
import org.springframework.stereotype.Repository;

@Repository
public interface RecordCarWithPersonInfoMapper extends BaseMapper<RecordCarWithPersonInfo> {
}
