package com.yuan.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yuan.pojo.SysUserPost;
import org.springframework.stereotype.Repository;

/*
 *  @author 雨安
 *  类名： SysUserPostMapper
 *  创建时间：2024/6/19
 */
@Repository
public interface SysUserPostMapper extends BaseMapper<SysUserPost> {
}
