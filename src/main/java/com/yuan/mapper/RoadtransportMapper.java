package com.yuan.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yuan.pojo.车辆基础信息.Roadtransport;
import org.springframework.stereotype.Repository;

/*
 *  @author 雨安
 *  类名： RoadtransportMapper
 *  创建时间：2024/3/29
 */
@Repository
public interface RoadtransportMapper extends BaseMapper<Roadtransport> {
}
