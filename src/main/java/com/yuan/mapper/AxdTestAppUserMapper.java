package com.yuan.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yuan.pojo.AxdTestAppUser;
import org.springframework.stereotype.Repository;

/*
 *  @author 雨安
 *  类名： AxdAppUserMapper
 *  创建时间：2024/3/17
 */
@Repository
public interface AxdTestAppUserMapper extends BaseMapper<AxdTestAppUser> {
}
