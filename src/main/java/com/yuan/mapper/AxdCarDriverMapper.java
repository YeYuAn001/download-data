package com.yuan.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yuan.pojo.AxdCarDriver;
import org.springframework.stereotype.Repository;

@Repository
public interface AxdCarDriverMapper extends BaseMapper<AxdCarDriver> {
}
