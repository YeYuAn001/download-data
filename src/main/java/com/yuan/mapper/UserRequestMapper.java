package com.yuan.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yuan.pojo.UserRequest;
import org.springframework.stereotype.Repository;

/*
 *  @author 雨安
 *  类名： UserRequestMapper
 *  创建时间：2024/3/15
 */
@Repository
public interface UserRequestMapper extends BaseMapper<UserRequest> {
}
