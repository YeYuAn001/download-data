package com.yuan.mapper.企业;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yuan.pojo.企业.AxdFirm;
import org.springframework.stereotype.Repository;

/*
 *  @author 雨安
 *  类名： AxdTestFirmMapper
 *  创建时间：2024/3/12
 */
@Repository
public interface AxdFirmMapper extends BaseMapper<AxdFirm> {
}
