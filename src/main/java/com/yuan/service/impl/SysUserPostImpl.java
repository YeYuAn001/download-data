package com.yuan.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yuan.mapper.SysUserPostMapper;
import com.yuan.pojo.SysUserPost;
import com.yuan.service.SysUserPostService;
import org.springframework.stereotype.Service;

/*
 *  @author 雨安
 *  类名： SysUserPostImpl
 *  创建时间：2024/6/19
 */
@Service
public class SysUserPostImpl extends ServiceImpl<SysUserPostMapper, SysUserPost> implements SysUserPostService {
}
