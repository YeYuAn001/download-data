package com.yuan.service.impl.车辆;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yuan.mapper.车辆.AxdCarTechnologyInfoMapper;
import com.yuan.pojo.车辆基础信息.AxdCarTechnologyInfo;
import com.yuan.service.车辆.AxdCarTechnologyInfoService;
import org.springframework.stereotype.Service;

/*
 *  @author 雨安
 *  类名： AxdCarTechnologyInfoImpl
 *  创建时间：2024/3/29
 */
@Service
public class AxdCarTechnologyInfoImpl extends ServiceImpl<AxdCarTechnologyInfoMapper, AxdCarTechnologyInfo> implements AxdCarTechnologyInfoService {
}
