package com.yuan.service.impl.车辆;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yuan.mapper.车辆.AxdCarBaseInfoMapper;
import com.yuan.pojo.车辆基础信息.AxdCarBaseInfo;
import com.yuan.service.车辆.AxdCarBaseInfoService;
import org.springframework.stereotype.Service;

/*
 *  @author 雨安
 *  类名： AxdCarBaseInfoImpl
 *  创建时间：2024/3/28
 */
@Service
public class AxdCarBaseInfoImpl extends ServiceImpl<AxdCarBaseInfoMapper, AxdCarBaseInfo> implements AxdCarBaseInfoService {
}
