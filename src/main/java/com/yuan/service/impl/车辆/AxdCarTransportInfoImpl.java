package com.yuan.service.impl.车辆;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yuan.mapper.车辆.AxdCarTransportInfoMapper;
import com.yuan.pojo.车辆基础信息.AxdCarTransportInfo;
import com.yuan.service.车辆.AxdCarTransportInfoServicce;
import org.springframework.stereotype.Service;

/*
 *  @author 雨安
 *  类名： AxdCarTransportInfoImpl
 *  创建时间：2024/3/29
 */
@Service
public class AxdCarTransportInfoImpl extends ServiceImpl<AxdCarTransportInfoMapper, AxdCarTransportInfo> implements AxdCarTransportInfoServicce {
}
