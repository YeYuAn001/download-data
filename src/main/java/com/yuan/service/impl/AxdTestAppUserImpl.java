package com.yuan.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yuan.mapper.AxdTestAppUserMapper;
import com.yuan.pojo.AxdTestAppUser;
import com.yuan.service.AxdTestAppUserService;
import org.springframework.stereotype.Service;

/*
 *  @author 雨安
 *  类名： AxdAppUserImpl
 *  创建时间：2024/3/17
 */
@Service
public class AxdTestAppUserImpl extends ServiceImpl<AxdTestAppUserMapper, AxdTestAppUser> implements AxdTestAppUserService {
}
