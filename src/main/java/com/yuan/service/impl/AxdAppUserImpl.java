package com.yuan.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yuan.mapper.AxdAppUserMapper;
import com.yuan.pojo.AxdAppUser;
import com.yuan.service.AxdAppUserService;
import org.springframework.stereotype.Service;

/*
 *  @author 雨安
 *  类名： AxdAppUserImpl
 *  创建时间：2024/6/19
 */
@Service
public class AxdAppUserImpl extends ServiceImpl<AxdAppUserMapper, AxdAppUser> implements AxdAppUserService {
}
