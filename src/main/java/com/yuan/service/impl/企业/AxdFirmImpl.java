package com.yuan.service.impl.企业;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yuan.mapper.企业.AxdFirmMapper;
import com.yuan.pojo.企业.AxdFirm;
import com.yuan.service.企业.AxdFirmService;
import org.springframework.stereotype.Service;

/*
 *  @author 雨安
 *  类名： AxdFirmImpl
 *  创建时间：2024/3/28
 */
@Service
public class AxdFirmImpl extends ServiceImpl<AxdFirmMapper, AxdFirm> implements AxdFirmService {
}
