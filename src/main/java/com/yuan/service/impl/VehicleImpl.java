package com.yuan.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yuan.dataupload.Vehicle;
import com.yuan.mapper.VehicleMapper;
import com.yuan.service.VehicleService;
import org.springframework.stereotype.Service;

/*
 *  @author 雨安
 *  类名： VehicleImpl
 *  创建时间：2024/6/3
 */
@Service
public class VehicleImpl extends ServiceImpl<VehicleMapper, Vehicle> implements VehicleService {
}
