package com.yuan.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yuan.mapper.UserRequestMapper;
import com.yuan.pojo.UserRequest;
import com.yuan.service.UserRequestService;
import org.springframework.stereotype.Service;

/*
 *  @author 雨安
 *  类名： UserRequestImpl
 *  创建时间：2024/3/15
 */
@Service
public class UserRequestImpl extends ServiceImpl<UserRequestMapper, UserRequest> implements UserRequestService {
}
