package com.yuan.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yuan.mapper.PractitionerMapper;
import com.yuan.pojo.Practitioner;
import com.yuan.service.PractitionerService;
import org.springframework.stereotype.Service;

/*
 *  @author 雨安
 *  类名： PractitionerImpl
 *  创建时间：2024/3/18
 */
@Service
public class PractitionerImpl extends ServiceImpl<PractitionerMapper,Practitioner>implements PractitionerService {
}
