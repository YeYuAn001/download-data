package com.yuan.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yuan.mapper.企业.AxdFirmMapper;
import com.yuan.pojo.企业.AxdFirm;
import com.yuan.service.AxdTestFirmService;
import org.springframework.stereotype.Service;

/*
 *  @author 雨安
 *  类名： AxdTestFirmImpl
 *  创建时间：2024/3/12
 */
@Service
public class AxdTestFirmImpl extends ServiceImpl<AxdFirmMapper, AxdFirm> implements AxdTestFirmService {
}
