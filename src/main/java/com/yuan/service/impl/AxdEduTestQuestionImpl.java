package com.yuan.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yuan.mapper.AxdEduTestQuestionMapper;
import com.yuan.pojo.AxdEduTestQuestion;
import com.yuan.service.AxdEduTestQuestionService;
import org.springframework.stereotype.Service;

@Service
public class AxdEduTestQuestionImpl extends ServiceImpl<AxdEduTestQuestionMapper, AxdEduTestQuestion> implements AxdEduTestQuestionService {
}
