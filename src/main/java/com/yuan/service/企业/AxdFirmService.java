package com.yuan.service.企业;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yuan.pojo.企业.AxdFirm;

/*
 *  @author 雨安
 *  类名： AxdFirmService
 *  创建时间：2024/3/28
 */
public interface AxdFirmService extends IService<AxdFirm> {
}
