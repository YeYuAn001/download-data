package com.yuan.service.车辆;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yuan.pojo.车辆基础信息.AxdCarBaseInfo;

/*
 *  @author 雨安
 *  类名： AxdCarBaseInfoService
 *  创建时间：2024/3/28
 */
public interface AxdCarBaseInfoService extends IService<AxdCarBaseInfo> {
}
