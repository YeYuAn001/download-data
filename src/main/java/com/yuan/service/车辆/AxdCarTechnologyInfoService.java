package com.yuan.service.车辆;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yuan.pojo.车辆基础信息.AxdCarTechnologyInfo;

/*
 *  @author 雨安
 *  类名： AxdCarTechnologyInfoService
 *  创建时间：2024/3/29
 */
public interface AxdCarTechnologyInfoService extends IService<AxdCarTechnologyInfo> {
}
