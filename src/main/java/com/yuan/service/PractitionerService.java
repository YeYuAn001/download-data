package com.yuan.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yuan.pojo.Practitioner;

/*
 *  @author 雨安
 *  类名： PractitionerService
 *  创建时间：2024/3/18
 */
public interface PractitionerService extends IService<Practitioner> {
}
