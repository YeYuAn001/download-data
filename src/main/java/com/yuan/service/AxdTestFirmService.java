package com.yuan.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yuan.pojo.企业.AxdFirm;

/*
 *  @author 雨安
 *  类名： AxdTestFirmService
 *  创建时间：2024/3/12
 */
public interface AxdTestFirmService extends IService<AxdFirm> {
}
