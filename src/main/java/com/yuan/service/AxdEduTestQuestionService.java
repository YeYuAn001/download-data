package com.yuan.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yuan.pojo.AxdEduTestQuestion;

public interface AxdEduTestQuestionService extends IService<AxdEduTestQuestion> {
}
