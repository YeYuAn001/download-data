package com.yuan.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yuan.dataupload.Vehicle;

/*
 *  @author 雨安
 *  类名： VehicleService
 *  创建时间：2024/6/3
 */
public interface VehicleService extends IService<Vehicle> {
}
