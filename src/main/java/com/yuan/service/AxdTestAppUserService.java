package com.yuan.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yuan.pojo.AxdTestAppUser;

/*
 *  @author 雨安
 *  类名： AxdAppUserService
 *  创建时间：2024/3/17
 */
public interface AxdTestAppUserService extends IService<AxdTestAppUser> {
}
