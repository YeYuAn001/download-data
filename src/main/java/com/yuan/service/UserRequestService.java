package com.yuan.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yuan.pojo.UserRequest;

/*
 *  @author 雨安
 *  类名： UserRequestService
 *  创建时间：2024/3/15
 */
public interface UserRequestService extends IService<UserRequest> {
}
