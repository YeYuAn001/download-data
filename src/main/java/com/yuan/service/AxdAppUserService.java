package com.yuan.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yuan.pojo.AxdAppUser;

/*
 *  @author 雨安
 *  类名： AxdAppUserService
 *  创建时间：2024/6/19
 */
public interface AxdAppUserService extends IService<AxdAppUser> {
}
