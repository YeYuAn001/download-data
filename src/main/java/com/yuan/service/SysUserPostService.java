package com.yuan.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yuan.pojo.SysUserPost;

/*
 *  @author 雨安
 *  类名： SysUserPostService
 *  创建时间：2024/6/19
 */
public interface SysUserPostService extends IService<SysUserPost> {
}
